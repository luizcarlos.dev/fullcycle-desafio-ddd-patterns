import EventHandlerInterface from "../../../@shared/event/event-handler.interface";
import Customer from "../../entity/customer";
import CustomerChangeAddressEvent from "../customer-change-address.event";
import CustomerCreatedEvent from "../customer-created.event";

export default class EnviaConsoleLogHandler implements EventHandlerInterface<CustomerChangeAddressEvent> {

  handle(event: CustomerChangeAddressEvent): void {
    const customer = <Customer> event.eventData;
    console.log(`Endereço do cliente: ID Customer: ${customer.id}, Customer: ${customer.name}, alterado para: ${customer.Address.toString()}`); 
  }

}
